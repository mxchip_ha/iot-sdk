/**
 ******************************************************************************
 * @file    alicloud_ilop.h
 * @author  QQ Ding
 * @version V1.0.0
 * @date    3-Step-2018
 * @brief   AliCloud ILOP service functions and framework header file
 ******************************************************************************
 *
 * Copyright (c) 2009-2018 MXCHIP Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

#ifndef _ALICLOUD_ILOP_H_
#define _ALICLOUD_ILOP_H_

#include "emh_api.h"

/** \addtogroup protocols */
/** @{*/

/** \addtogroup Alicloud_ILOP_Service */
/** @{*/

/******************************************************************************
 *                                   Macros
 ******************************************************************************/

#define ilop_log(M, ...) MX_LOG(CONFIG_CLOUD_DEBUG, "ILOP", M, ##__VA_ARGS__)

/******************************************************************************
 *                                 Constants
 ******************************************************************************/

/******************************************************************************
 *                                 Enumerations
 ******************************************************************************/

/** Alicloud ilop service events */
enum ilop_status_e {
    ILOP_STATUS_WLAN_CONFIG_STARTED, /**< AWS service is started to set wlan and cloud. */
    ILOP_STATUS_WLAN_CONNECTED, /**< Device is connected to Wi-Fi access point. */
    ILOP_STATUS_WLAN_DISCONNECTED, /**< Device is disconnected from Wi-Fi access point. */
    ILOP_STATUS_CLOUD_CONNECTED, /**< Alicloud ilop service is connected */
    ILOP_STATUS_CLOUD_DISCONNECTED, /**< Alicloud ilop service is disconnected. */
};
typedef uint8_t ilop_status_t; /**< Alicloud ilop service events */

enum ilop_cb_e {
    ILOP_CB_STATUS,
    ILOP_CB_PROPERTY_GET,
    ILOP_CB_PROPERTY_SET,
    ILOP_CB_SERVICE_SET,
    ILOP_CB_RAWDATA_SET,
};
typedef uint8_t ilop_cb_t;

/******************************************************************************
 *                               Type Definitions
 ******************************************************************************/

typedef struct _ilop_device_key_t {
    char product_key[EMH_ILOP_PRODUCT_KEY_MAXLEN];
    char product_secret[EMH_ILOP_PRODUCT_SECRET_MAXLEN];
    char device_name[EMH_ILOP_DEVICE_NAME_MAXLEN];
    char device_secret[EMH_ILOP_DEVICE_SECRET_MAXLEN];
} ilop_device_key_t;

/******************************************************************************
 *                             Function Declarations
 ******************************************************************************/

/**
 * @brief	        Initialize EMW module with product info registered on cloud
 *                  console.
 * 
 * @param[in]       config       : ILOP product info registered on cloud console
 *
 * @return      	mx_status
 *
 */
mx_status ilop_init(emh_arg_ilop_domain_t domain);

/**
 *
 * @brief        	Alicould ilop service runloop, application should called
 *                  periodically to handle events and transfer data.
 *                  To save power, also can be called when uart data is ready 
 *                  to receive or sensor data is ready to send to cloud
 * @note            Never call this function in event handler
 * 
 * @return      	mx_status
 *
 */
mx_status ilop_runloop(void);

/**
 * 
 * @brief 	Start AWSS Wi-Fi configuration. EMW module start monitor mode
 * 
 * @return	mx_status
 *
 */
mx_status ilop_awss_start(void);


/**
 *
 * @brief	Try to procedure an unbind operation on cloud if connected and 
 *          restore module settings to default.
 * 
 * @return	mx_status
 *
 */
mx_status ilop_restore(void);

/**
 * @brief		Set product key, product secret, device secret, device name is used to connect to ILOP service.
 * @note  		The default key is stored in EMW module, use this function to write the new key.
 * 
 * @param[in] 	key: see ilop_device_key_t
 * 
 * @return		status
 */
mx_status ilop_set_device_key(const ilop_device_key_t* key);

void ilop_register_callback(ilop_cb_t cb_type, void* cb_func);

mx_status ilop_report_property(char* data, uint32_t len);
mx_status ilop_report_event(char* event_id, char* data, uint32_t len);
mx_status ilop_retport_rawdata(uint8_t *data, uint32_t len);
#endif //_ALICLOUD_ILOP_H_

/** @}*/
/** @}*/
