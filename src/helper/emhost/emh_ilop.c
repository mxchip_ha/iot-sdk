/**
 ******************************************************************************
 * @file    emh_ilop.c
 * @author  QQ Ding
 * @version V1.0.0
 * @date    3-Sept-2018
 * @brief   Alicloud ILOP service AT commands API
 ******************************************************************************
 *
 * Copyright (c) 2009-2018 MXCHIP Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */


#include <stdlib.h>
#include <string.h>

#include "emh_api.h"
#include "ATCmdParser/ATCmdParser.h"



/******************************************************************************
 *                              Function Definitions
 ******************************************************************************/
// MX_WEAK void emh_ev_ilop_connection(emh_arg_ilop_conn_t conn)
// {
//     printf("conn=%d\r\n", conn);
//     return;
// }

// MX_WEAK void emh_ev_ilop_set_local_attr(emh_ilop_msg *msg)
// {
//     if( msg->format == EMH_ARG_ILOP_FORMAT_ICA){
//         printf("msg=%s\r\n", msg->data);
//     }
//     return;
// }

mx_status emh_ilop_set_domain( emh_arg_ilop_domain_t domain )
{
    char arg[10];

    //check domain
    if (!(ATCmdParser_send("AT+ILOPDOMAIN?")
	   && ATCmdParser_recv("+ILOPDOMAIN:%10[^\r]\r\nOK\r\n",arg))) {
		return kReadErr;
	}

    //if dm value different, we need to be set
    if( emh_arg_for_arg( EMH_ARG_ILOP_DOMAIN, arg) != domain )
    {
        if ( !(ATCmdParser_send("AT+ILOPDOMAIN=%s", emh_arg_for_type(EMH_ARG_ILOP_DOMAIN, domain))
	        && ATCmdParser_recv("OK\r\n"))) {
		    return kReadErr;
	    }
    }

    return kNoErr;
}

mx_status emh_ilop_set_key( const char *pk, const char *ps, const char *ds, const char *dn )
{
    if (ATCmdParser_send("AT+ILOPSET=%s,%s,%s,%s", pk, ps, ds, dn)
	 && ATCmdParser_recv("OK\r\n")) {
		return kNoErr;
	}
	return kGeneralErr;
}

mx_status emh_ilop_get_key( char pk[EMH_ILOP_PRODUCT_KEY_MAXLEN], char ps[EMH_ILOP_PRODUCT_SECRET_MAXLEN], char ds[EMH_ILOP_DEVICE_SECRET_MAXLEN], char dn[EMH_ILOP_DEVICE_NAME_MAXLEN] )
{
    char args[200], *arg_list[5];

	if (!(ATCmdParser_send("AT+ILOPSET?")
	   && ATCmdParser_recv("+ILOPSET:%200[^\r]\r\nOK\r\n",args))) {
		return kReadErr;
	}

    if (4 != ATCmdParser_analyse_args(args, arg_list, 4)) {
		return kMalformedErr;
	}

    strncpy( pk, arg_list[0], EMH_ILOP_PRODUCT_KEY_MAXLEN );
    strncpy( ps, arg_list[1], EMH_ILOP_PRODUCT_SECRET_MAXLEN );
    strncpy( ds, arg_list[2], EMH_ILOP_DEVICE_SECRET_MAXLEN );
    strncpy( dn, arg_list[3], EMH_ILOP_DEVICE_NAME_MAXLEN );

    return kNoErr;
}

mx_status emh_ilop_awss_start( void )
{
    if (ATCmdParser_send("AT+ILOPAWSSTART")
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }
	return kGeneralErr;
}

mx_status emh_ilop_awss_stop( void )
{
    if (ATCmdParser_send("AT+ILOPAWSSTOP")
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }
	return kGeneralErr;
}

mx_status emh_ilop_service_start( void )
{
    if (ATCmdParser_send("AT+ILOPSTART")
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }
	return kGeneralErr;
}

emh_arg_ilop_status_t emh_ilop_get_stauts( void )
{
	char arg[20];

	if (!(ATCmdParser_send("AT+ILOPSTATUS?")
	   && ATCmdParser_recv("+ILOPSTATUS:%20[^\r]\r\nOK\r\n",arg))) {
		return EMH_ARG_ERR;
	}
	
	return emh_arg_for_arg( EMH_ARG_ILOP_STATUS, arg);
}

mx_status emh_ilop_unbind( void )
{
	if (ATCmdParser_send("AT+ILOPRESET")
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }
	return kGeneralErr;
}

mx_status emh_ilop_service_stop( void )
{
    if (ATCmdParser_send("AT+ILOPSTOP")
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }
	return kGeneralErr;
}

mx_status emh_ilop_send_prop_to_cloud( char *data, uint32_t len )
{
    if (ATCmdParser_send("AT+ILOPSENDJSON=property,%d", len)
    //  && ATCmdParser_recv(">")
	 && ATCmdParser_write(data, len) == len
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }

	return kGeneralErr;
}

mx_status emh_ilop_send_event_to_cloud( char *event_id, char *data, uint32_t len )
{
    if (ATCmdParser_send("AT+ILOPSENDJSON=event,%s,%d", event_id, len)
    //  && ATCmdParser_recv(">")
	 && ATCmdParser_write(data, len) == len
     && ATCmdParser_recv("OK\r\n")) {
        return kNoErr;
    }

	return kGeneralErr;
}

mx_status emh_ilop_send_raw_to_cloud( uint8_t *data, uint32_t len )
{
    if (ATCmdParser_send("AT+ILOPSENDRAW=%d", len)
	//  && ATCmdParser_recv(">")
	 && ATCmdParser_write((char *)data, len) == len
	 && ATCmdParser_recv("OK\r\n")) {
		return kNoErr;
	}
	return kGeneralErr;
}

void emh_ilop_event_handler(void)
{
    mx_status err = kNoErr;
    char arg1[10], arg2[20];
    emh_arg_ilop_conn_t conn;
    emh_arg_ilop_vt_t vt;
    emh_ilop_msg msg;

    msg.data = NULL;

    require_action(ATCmdParser_recv("%10[^,],", arg1), exit, err = kMalformedErr);

    emh_arg_ilop_ev_t event = emh_arg_for_arg(EMH_ARG_ILOP_EV, arg1);
    require_action(event != EMH_ARG_ERR, exit, err = kMalformedErr);

    if (event == EMH_ARG_ILOP_EV_ILOP) { /* ILOP Server connection event */
        require_action(ATCmdParser_recv("%20[^\r]\r\n", arg2), exit, err = kMalformedErr);
        conn = emh_arg_for_arg(EMH_ARG_ILOP_CONN, arg2);
        require_action(conn != EMH_ARG_ERR, exit, err = kMalformedErr);
        emh_ev_ilop_connection(conn);
        goto exit;
    } else if (event == EMH_ARG_ILOP_EV_GETJSON) {
        emh_ev_ilop_get_local_attr();
        goto exit;
    } else if (event == EMH_ARG_ILOP_EV_SETJSON) { /* ILOP server === JSON value===> device */
        require_action(ATCmdParser_recv("%20[^,],", arg2), exit, err = kMalformedErr);
        vt = emh_arg_for_arg(EMH_ARG_ILOP_VT, arg2);
        require_action(vt != EMH_ARG_ERR, exit, err = kMalformedErr);

        if (vt == EMH_ARG_ILOP_VT_PROPERTY) {
            msg.format = EMH_ARG_ILOP_FORMAT_JSON_PROPERTY;
        } else if (vt == EMH_ARG_ILOP_VT_SERVICE) {
            msg.format = EMH_ARG_ILOP_FORMAT_JSON_SERVICE;
        }
    } else if (event == EMH_ARG_ILOP_EV_SETRAW) { /* ILOP server === RAW value===> device */
        msg.format = EMH_ARG_ILOP_FORMAT_RAW;
    }

    if ((event == EMH_ARG_ILOP_EV_SETJSON) || (event == EMH_ARG_ILOP_EV_SETRAW)) {
        /* Read package data */
        uint32_t count = 0;
        require_action(ATCmdParser_recv("%d,", &count), exit, err = kMalformedErr);

        msg.data = malloc(count+3);
        require_action(msg.data, exit, err = kNoMemoryErr);
        
        require_action(ATCmdParser_read((char*)msg.data, count+2) == count+2, exit, err = kTimeoutErr);

        msg.data[count] = '\0';
        msg.len = count;
        emh_ev_ilop_set_local_attr(&msg);
    }

exit:
    if (msg.data != NULL) {
        free(msg.data);
    }

    if (err == kMalformedErr) {
        emh_ev_unknown();
    }

    return;
}
