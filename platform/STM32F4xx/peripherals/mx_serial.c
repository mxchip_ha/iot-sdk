/**
 ******************************************************************************
 * @file    mx_serial.c
 * @author  QQ Ding
 * @version V1.0.0
 * @date    3-Sept-2018
 * @brief   UART driver used for AT parser
 ******************************************************************************
 *
 * Copyright (c) 2009-2018 MXCHIP Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */


#include "mx_common.h"
#include "mx_hal.h"
#include "mx_ringbuffer.h"

/******************************************************************************
 *                              Variable Definitions
 ******************************************************************************/

static int _timeout = 100;
static uint8_t at_buffer[MX_SERIAL_RX_BUF_SIZE];
static struct ringbuffer at_rx;
//static pthread_mutex_t at_mtx;

/******************************************************************************
 *                               Type Definitions
 ******************************************************************************/

static uart_dev_t uart_dev = {
    AT_UART_PORT,
    {
        AT_UART_BAUDRATE,
        DATA_WIDTH_8BIT,
        NO_PARITY,
        STOP_BITS_1,
        FLOW_CONTROL_DISABLED,
        MODE_TX_RX,
    },
};

/******************************************************************************
 *                              Function Definitions
 ******************************************************************************/
static int32_t at_async_read(uint8_t* const buf, const uint16_t length)
{
    uint16_t was_read = 0;
    uint32_t num;

    if (buf == 0 || length == 0)
        return 0;

    CRITICAL_SECTION_ENTER()
    num = ringbuffer_num(&at_rx);
    CRITICAL_SECTION_LEAVE()
//    printf("+[%d]",num);

    while ((was_read < num) && (was_read < length)) {
        ringbuffer_get(&at_rx, &buf[was_read++]);
    }

    return (int32_t)was_read;
}

void mx_hal_serial_init(int timeout)
{
    mx_status err;
    _timeout = timeout;

    ringbuffer_init(&at_rx, at_buffer, MX_SERIAL_RX_BUF_SIZE);

    err = mx_hal_uart_init(&uart_dev);
    require_noerr(err, exit);
    mx_hal_serial_iaq_set( );   
exit:
    return;
}

void mx_hal_serial_set_timeout(int timeout)
{
    _timeout = timeout;
}

int mx_hal_serial_putc(char c)
{
    return mx_hal_uart_send(&uart_dev, &c, 1, _timeout);
}

int mx_hal_serial_getc(void)
{
    uint32_t current = mx_hal_ms_ticker_read();
    uint8_t ch;
    
//    printf("_timeout:%d\r\n", _timeout);

    do {
        if (at_async_read(&ch, 1) == 1)
        {
//          printf("-ch[%02x]\r\n",ch);
            return ch;
        }
//        printf("time:%d\r\n", (mx_hal_ms_ticker_read() - current));
    } while ((mx_hal_ms_ticker_read() - current) < _timeout);

    return -1;
}

bool mx_hal_serial_readable(void)
{
    if (ringbuffer_num(&at_rx))
        return true;
    return false;
}

void mx_hal_serial_flush(void)
{
    uint32_t num;
    uint8_t tmp; 
    for (num = ringbuffer_num(&at_rx); num > 0; num--) {
        ringbuffer_get(&at_rx, &tmp);
    }

}

void set_serial_ringbuffer( uint8_t data)
{
  CRITICAL_SECTION_ENTER()
  ringbuffer_put(&at_rx, data);
  CRITICAL_SECTION_LEAVE()
}