/**
 ******************************************************************************
 * @file    mx_tick.c
 * @author  QQ Ding
 * @version V1.0.0
 * @date    3-Sept-2018
 * @brief   Tick for delay and timeout
 ******************************************************************************
 *
 * Copyright (c) 2009-2018 MXCHIP Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

#include <time.h>
#include "mx_common.h"
#include "stm32f4xx_hal.h"
/******************************************************************************
 *                              Variable Definitions
 ******************************************************************************/

/******************************************************************************
 *                              Function Definitions
 ******************************************************************************/

void mx_hal_ms_ticker_init(void)
{
    return;
}

uint32_t mx_hal_ms_ticker_read(void)
{
    uint32_t ms;
    ms = HAL_GetTick();
//    printf("-[%d]\r\n",ms);
    return ms;
}

void mx_hal_delay_ms(volatile uint32_t delay)
{
    HAL_Delay(delay);
}
