/***
 * File: retarget.c
 * porting iar project to keil, something need to do.
 *
 * Created by JerryYu @ Jan 13rd,2015
 * Ver: 0.1
 * */

#include <string.h> /* strlen */
#include <stdio.h>  /* snprintf */
#include "usart.h"
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef at_uart;//huart1
extern UART_HandleTypeDef stdio_uart;//huart2
/********重写fputc********/
int fputc(int ch, FILE *f)
{
     unsigned char temp[1]={ch};
     HAL_UART_Transmit( &stdio_uart, temp, 1, 0x02 );
//   HAL_UART_Transmit( &huart2, ( uint8_t* )&ch, 1, 0 );
    return (ch);
}

/********重写fgetc********/
int fgetc(FILE *f)
{
    unsigned char  ch;
    HAL_UART_Receive(&stdio_uart,(uint8_t *)&ch, 1, 0);
    return  ch;
}
