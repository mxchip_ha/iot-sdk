/**
 ******************************************************************************
 * @file    mx_uart.c
 * @author  QQ Ding
 * @version V1.0.0
 * @date    3-Sept-2018
 * @brief   UART driver function
 ******************************************************************************
 *
 * Copyright (c) 2009-2018 MXCHIP Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

#include "mx_common.h"
#include "mx_hal.h"
#include "stm32f4xx_hal.h"
#include "gpio.h"

/******************************************************************************
 *                              Variable Definitions
 ******************************************************************************/
UART_HandleTypeDef at_uart; //huart1
UART_HandleTypeDef stdio_uart; //huart2
uint8_t aTxBuffer[]="\r\n12345678\r\n";
/******************************************************************************
 *                              Function Definitions
 ******************************************************************************/


void HAL_UART_RX_USART1( uint8_t data_received )
{
    set_stdio_ringbuffer(data_received);
}

void HAL_UART_RX_USART2( uint8_t data_received )
{
    set_serial_ringbuffer(data_received);
}

void mx_hal_uart_dev_set(char* dev)
{
    return;
}

int32_t mx_hal_uart_init(uart_dev_t* uart)
{
    UART_HandleTypeDef* temp_uart;

    if (uart->port == AT_UART_PORT) {
        temp_uart = (UART_HandleTypeDef*)&at_uart;
        temp_uart->Instance = USART2;
    } else if (uart->port == STDIO_UART_PORT) {
        temp_uart = (UART_HandleTypeDef*)&stdio_uart;
        temp_uart->Instance = USART1;
    }
    
    temp_uart->Init.BaudRate = uart->config.baud_rate;
    
    if (uart->config.data_width == DATA_WIDTH_8BIT) {
        temp_uart->Init.WordLength = UART_WORDLENGTH_8B;
    } else if (uart->config.data_width == DATA_WIDTH_9BIT) {
        temp_uart->Init.WordLength = UART_WORDLENGTH_9B;
    }

    if (uart->config.stop_bits == STOP_BITS_1) {
        temp_uart->Init.StopBits = UART_STOPBITS_1;
    } else if (uart->config.stop_bits == STOP_BITS_2) {
        temp_uart->Init.StopBits = UART_STOPBITS_2;
    }

    if (uart->config.parity == NO_PARITY) {
        temp_uart->Init.Parity = UART_PARITY_NONE;
    } else if (uart->config.parity == ODD_PARITY) {
        temp_uart->Init.Parity = UART_PARITY_ODD;
    } else if (uart->config.parity == EVEN_PARITY) {
        temp_uart->Init.Parity = UART_PARITY_EVEN;
    }

    if (uart->config.mode == MODE_TX_RX) {
        temp_uart->Init.Mode = UART_MODE_TX_RX;
    } else if (uart->config.mode == MODE_RX) {
        temp_uart->Init.Mode = UART_MODE_RX;
    } else if (uart->config.mode == MODE_TX) {
        temp_uart->Init.Mode = UART_MODE_TX;
    }

    if (uart->config.flow_control == FLOW_CONTROL_DISABLED) {
        temp_uart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
    } else if (uart->config.flow_control == FLOW_CONTROL_CTS) {
        temp_uart->Init.HwFlowCtl = UART_HWCONTROL_CTS;
    } else if (uart->config.flow_control == FLOW_CONTROL_RTS) {
        temp_uart->Init.HwFlowCtl = UART_HWCONTROL_RTS;
    } else if (uart->config.flow_control == FLOW_CONTROL_CTS_RTS) {
        temp_uart->Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
    }

    temp_uart->Init.OverSampling = UART_OVERSAMPLING_16; //

    if (HAL_UART_Init(temp_uart) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
    

//      uart_it_test();
    // clear uart buffer
return 0;
}

void mx_hal_stdio_iaq_set( void )
{   
    SET_BIT( stdio_uart.Instance->CR3, USART_CR3_EIE );
    /* Enable the UART Parity Error interrupt and Data Register Not Empty interrupt */
    SET_BIT( stdio_uart.Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE );  
}

void mx_hal_serial_iaq_set( void )
{   
    SET_BIT( at_uart.Instance->CR3, USART_CR3_EIE );
    /* Enable the UART Parity Error interrupt and Data Register Not Empty interrupt */
    SET_BIT( at_uart.Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE );  
}

int32_t mx_hal_uart_send(uart_dev_t* uart, const void* data, uint32_t size, uint32_t timeout)
{
    uint32_t ret, rmd = size;

    if (uart->port == AT_UART_PORT) {
        ret = HAL_UART_Transmit(&at_uart, (uint8_t*)data, rmd, timeout);
//        while(__HAL_UART_GET_FLAG(&at_uart,UART_FLAG_TC)!=SET);	
        return ret;
    } else if (uart->port == STDIO_UART_PORT) {
        ret = HAL_UART_Transmit(&stdio_uart, (uint8_t*)data, rmd, timeout);
//        while(__HAL_UART_GET_FLAG(&stdio_uart,UART_FLAG_TC)!=SET);	
        return ret;
    }

    return 0;
}

int32_t mx_hal_uart_recv(uart_dev_t* uart, void* data, uint32_t expect_size, uint32_t* recv_size, uint32_t timeout)
{
    return 0;
}

int32_t mx_hal_uart_finalize(uart_dev_t* uart)
{
    //deinit
    return 0;
}
