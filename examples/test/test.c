#include "mx_common.h"
#include "mx_debug.h"

#include "emh_api.h"
#include "mx_hal.h"

#define APP_DEBUG MX_DEBUG_ON
#define app_log(M, ...) MX_LOG(APP_DEBUG, "APP", M, ##__VA_ARGS__)

void application_start(void)
{
    mx_status err = kNoErr;

    mx_hal_ms_ticker_init();
    mx_hal_stdio_init();

    err = emh_init();
    if (err == kNoErr) {
        app_log("FW version: %s", emh_module_get_fw_version());
        app_log("System tick: %d", (int)emh_module_get_tick());
    }

    while (1) {
        emh_runloop();
    }

    return;
}
