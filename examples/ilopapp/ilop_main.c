/**
 ******************************************************************************
 * @file    ilopmain.c
 * @author  QQ Ding
 * @version V1.0.0
 * @date    3-Sept-2018
 * @brief   ILOP Service Main function
 ******************************************************************************
 *
 * Copyright (c) 2009-2018 MXCHIP Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

#include "alicloud_ilop.h"
#include "mx_common.h"
#include "mx_debug.h"

#include "emh_api.h"
#include "ilop.h"
#include "mx_cli.h"
#include "mx_hal.h"

static int awss_start = 0;
static int reset = 0;

const ilop_device_key_t device_key = {
    .product_key = "a1MeLuIJPYL",
    .product_secret = "SegyIuWW6xBQGuIc",
    .device_name = "aQI0eEM5g2nlSr2njD0x",
    .device_secret = "o7EZShzl6z8F3xq8qF3vN9P7HFGslRAV",
};

static void user_cli_loop(void)
{
    if (awss_start == 1) {
        ilop_awss_start();
        awss_start = 0;
    }
    if (reset == 1) {
        ilop_restore();
        reset = 0;
    }
}

#ifdef MX_CLI_ENABLE
static void handle_aws_cmd(char* pwbuf, int blen, int argc, char** argv)
{
    if (argc != 2)
        return;

    if (strcmp(argv[1], "start") == 0) {
        awss_start = 1;
    }
}

static void handle_reset_cmd(char* pwbuf, int blen, int argc, char** argv)
{
    reset = 1;
}

static struct cli_command ilopcmds[] = {
    { "aws", "aws [start]", handle_aws_cmd },
    { "reset", "clean wifi module and ilop service", handle_reset_cmd },
};
#endif

void application_start(void)
{
    mx_status err = kNoErr;

    /* System initialization， ticker, stdio */
    mx_hal_ms_ticker_init();
    mx_hal_stdio_init();

#ifdef MX_CLI_ENABLE
    cli_register_commands(ilopcmds, sizeof(ilopcmds) / sizeof(struct cli_command));
#endif

    /* ILOP service initialization */
    err = ilop_init(EMH_ARG_ILOP_DOMAIN_SHANGHAI);
    if (err != kNoErr)
        app_log("ilop init err");

    /* Set the ILOP three tuple. If the module is pre burned, delete the function */
    // ilop_set_device_key(&device_key);

    ilop_register_callback(ILOP_CB_STATUS, (void *)user_ilop_status_cb);
    ilop_register_callback(ILOP_CB_SERVICE_SET, (void *)user_ilop_service_set_cb);
    ilop_register_callback(ILOP_CB_PROPERTY_SET, (void *)user_ilop_property_set_cb);
    ilop_register_callback(ILOP_CB_PROPERTY_GET, (void *)user_ilop_property_get_cb);
    ilop_register_callback(ILOP_CB_RAWDATA_SET, (void *)user_ilop_rawdata_set_cb);

    while (1) {
        ilop_runloop();
        user_cli_loop();
        user_ilop_loop();
    }
}
