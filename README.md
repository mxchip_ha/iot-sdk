# How to use iot-sdk


## 1. 简介
本 sdk 提供一个基于 Linux，通过与 Wi-Fi 模块进行 AT 指令串口通信，实现与阿里云 ILOP 和 sds 直连的 IoT 物联网典型开发应用示例源代码。

### 1.1 适用范围

- MX1290/MX1290V2
    - EMW080B
    - EMW5080

- MOC108
    - EMW3060
    - EMW3060B

## 2. 目录结构

```c
|---example
|   |---ilopapp     /*ilop平台的demo*/
|   |---sdsapp      /*sds平台的demo*/
|   |---test        /*测试demo*/
|
|---platform
|   |---linux       /*linux平台相关的接口实现*/
|   |---STM32F4xx   /*STM32平台相关的接口实现及驱动*/
|   |---mx_hal.h
|
|---src 
    |---alicloud_ilop   /*ilop平台的实现*/
    |---alicloud_sds    /*sds平台的实现*/
    |---helper          /*at指令解析内核*/
```

## 3. Linux 平台开发

### 3.1 准备

- 串口接入PC，并查询设备，命令：`ls -l /dev/ttyUSB*`

```
parallels@parallels-vm:~/iot-sdk$ ls -l /dev/ttyUSB*
crw-rw---- 1 root dialout 188, 0 Sep  3 16:15 /dev/ttyUSB0
```

### 3.2 编译
iot-sdk 提供四个demo，编译demo的命令格式 `make <demo>`

- 清除上次的make命令所产生的build文件，命令：`make clean`

```
parallels@parallels-vm:~/iot-sdk$ make clean
cleaning...
done
```

- 编译测试demo，命令：`make test`

```
parallels@parallels-vm:~/iot-sdk$ make test
mkdir build		
compiling platform/linux/mx_serial.c
compiling platform/linux/mx_stdio.c
compiling platform/linux/mx_tick.c
compiling platform/linux/mx_uart.c
compiling src/helper/cli/mx_cli.c
compiling src/helper/emhost/ATCmdParser/ATCmdParser.c
compiling src/helper/emhost/emh_alisds.c
compiling src/helper/emhost/emh_arg.c
compiling src/helper/emhost/emh_ilop.c
compiling src/helper/emhost/emh_module.c
compiling src/helper/emhost/emh_wlan.c
compiling src/helper/mx_utils/mx_ringbuffer.c
compiling examples/test/test.c
generating build/iot-test.elf
   text	   data	    bss	    dec	    hex	filename
  16294	    928	   3680	  20902	   51a6	build/iot-test.elf
```

- 编译完成后，在build目录下生成 `iot-test.elf` 文件

### 3.3 运行

- 运行build目录下的elf文件，命令：`./build/iot-test.elf <dev>`

```
parallels@parallels-vm:~/iot-sdk$ sudo ./build/iot-test.elf /dev/ttyUSB0
[sudo] password for parallels: 
dev[/dev/ttyUSB0]
open at uart succeed
[APP: test.c:  19] FW version: ilop_AT_v2.1.4
[APP: test.c:  20] System tick: 757
```

## 4. STM32平台开发

![image-20180930094133654](https://ws3.sinaimg.cn/large/006tNc79gy1fvrbknkomaj30v00l8wv7.jpg)

								图 1  `NUCLEO-F411RE` 开发板

说明：

- 	`Wi-Fi` 模块选用与   配套的  `Wi-Fi` 模块 套件 : `EXT-AT3080 V 2.0`
- 	移植平台选择 IAR : EWARM 版本是 `IAR Embedded Workbench for ARM 7.40.1.8472`
- 	选择 ST 官方的 代码生成工具: `STM32CubeMX` 


### 4.1 硬件准备以及说明

#### 4.1.1 选用 `stdio uart` 和 `serial uart`

选用了`NUCLEO-F411RE` 开发板的 `PA3(UART2_RX)` 、 `PA2(UART2_TX)` 作为 `serial uart`，也就是与Wi-Fi模块通讯的串口，选用了开发板的 `PA9(UART1_RX)` 、 `PA10(UART1_TX)` 作为 `stdio uart`。

需要注意的是 `NUCLEO-F411RE` 开发板默认 `PA3(UART2_RX)` 、 `PA2(UART2_TX)` 是与STLINK 的STLK_RX 和 STLK_TX 连接 ,需要断开 SB13 和 SB14 SB62 和 SB63 连接。

更多关于 `NUCLEO-F411RE` 开发板的的硬件资料可以访问 `ST`  官网：[NUCLEO-F411RE](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-eval-tools/stm32-mcu-eval-tools/stm32-mcu-nucleo/nucleo-f411re.html)



#### 4.1.1 `EXT-AT3080 V 2.0` 设置

烧录 庆科官网的 `ILOP`  `Wi-Fi` 固件。

`ILOP` 平台注册产品以及 AT 固件使用 参考：http://developer.mxchip.com/at2/108

![image-20180930094550757](https://ws4.sinaimg.cn/large/006tNc79gy1fvrbp29dbyj31h40f0aq7.jpg)



### 4.2 使用 `STM32CubeMX` 自动生成底层代码

基于 `NUCLEO-F411RE` 开发板使用了两组串口 。这部分代码可以自己实现。亦可以使用 `ST` 官方的自动代码生成工具`STM32CubeMX`  。关于 `STM32CubeMX`  的使用可以参考 `ST` 官网：[`STM32CubeMX`](https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-configurators-and-code-generators/stm32cubemx.html)

其次，需要重定向 `printf`， 重写 `fputc` 和 `fgetc`  两个函数。

生成之后 规范一下 代码结构，以方便移植。需要实现的是 `mx_hal.h` 里面的那些接口。



### 4.3 移植 `iot-sdk`

移植可以分为以下几步；

-  `example` 和 `src` 文件夹部分可以直接移植到 `NUCLEO-F411RE` 
-  platform 文件夹部分 是需要适配到 `NUCLEO-F411RE` ，需要去掉  `Linux`  下的 `Pthread`  、`mutex` ，  `Hal` 层 需改串口部分,实现 `mx_hal.h` 里面的 `api` 
-  根据使用情况修改堆栈大小，更改 `run` 入口：`application_start`，添加部分使用宏定义
-  移植可以先移植 `at_test`  , 然后 移植  `ilopicaapp`  和 `iloprawapp` 部分
-  修改 Workspace 的一些配置

下面进行详细说明。

### 4.4 移植 `example` 和 `src` 

1. 为了移植代码规范，修改了默认工程文件夹目录，更新为和 `iot-sdk` 相类四个文件夹：`EWARM` 、`Example` 、`Platform` 、`Src`

   - `EWARM` : IAR 相关的工程文件
   - `Example` : 移植 `iot-sdk/example` 下的文件
   - `Platform` :   `NUCLEO-F411RE`  平台相关的接口实现，`linux` 下使用了锁机制，使用
   - `Src` ：移植 `iot-sdk/src ` 下的文件

2. 本地的文件目录为：

   ```
   .
   ├── EWARM
   │   ├── at_host.dep
   │   ├── at_host.ewd
   │   ├── at_host.ewp
   │   ├── at_host.ewt
   │   ├── at_host.eww
   │   ├── at_test
   │   ├── at_test.dep
   │   ├── ilopicaapp
   │   ├── iloprawapp
   │   ├── settings
   │   ├── startup_stm32f411xe.s
   │   ├── stm32f411xe_flash.icf
   │   └── stm32f411xe_sram.icf
   ├── Example
   │   ├── at_test     /*测试demo*/
   │   ├── ilopicaapp  /*ilop平台 ica标准数据格式的demo*/
   │   └── iloprawapp  /*iloppingtai. */
   ├── Platform
   │   └── STM32F4xx   /*STM32F4xx 平台相关的接口实现*/
   ├── README.md
   ├── at_test.ioc     
   └── src
       ├── alicloud_ilop /*ilop平台的实现*/
       └── helper        /*at指令解析内核*/
   ```

3. `IAR`  工程部分的文件目录

   -  `at host porject`

     	![image-20180930152209546](https://ws1.sinaimg.cn/large/006tNc79gy1fvrlf1asqaj30ca0fqmz0.jpg)

   -  `ilopicaapp project`

     	![image-20180930152247654](https://ws4.sinaimg.cn/large/006tNc79gy1fvrlfnr8zlj30ca0fqac5.jpg)

   - `iloprawapp project` 

     	![image-20180930152325550](https://ws4.sinaimg.cn/large/006tNc79gy1fvrlgb6bk5j30ca0fqwgi.jpg)

   关于如何在同一个 `Workspace` 里面新建多个 `project` ，可以参见  ***`IAR` 工程设置说明***

4. `IAR` 工程设置说明	

   - 需要将更改之后新的目录以及 `IAR` 设置 头文件路径
   - 在 `at-host` 当前的 `Workspace` 里面 建立了 3 个 `project`,  可以在 `IAR-Project-Edit Confiigurations` 选项里面进行设置
   - `Options` - `General Options` -`Output` : `Output file`  选择 `Executable` , `Output directories`部分，设置 以 `ilopicaapp`  `project` 为例 `Executable/libraries` : `ilopicaapp\Exe`  , `Objecet files`：`ilopicaapp\Obj` ,   `List files` : `ilopicaapp\List`
   - `Options` - `General Options` -  `Library Options` :  `Printf formatter`  设置等级 `Full` ,`Scanf formatter` 设置等级 `Full without multibytes`
   - `Options` - `C/C++ Compiler` -  `Preprocessor` :  `Defined symbols` : `(one per line)`  
     - `at_host project` 增加如下宏:`MX_CLI_ENABLE_` 、`MX_DEBUG`
     - `ilopicaapp project` 增加如下宏:`MX_CLI_ENABLE` 、`AT_SUPPORT_ILOP`  、`ILOP_USE_ICA` 、`MX_DEBUG`
     - `iloprawapp  project` 增加如下宏:`MX_CLI_ENABLE` 、`AT_SUPPORT_ILOP` 、`ILOP_USE_RAW` 、`ILOP_USE_ICA` 、`MX_DEBUG`
   - `Debugger` - `Setup` :  `Driver` 设置为 `ST-LINK`  ,  选择  `Run to`  `application_start` 
   - `Linker` - `Config  Linker configuration files`  勾选 `Override default`, 点击 `Edit...` 根据使用情况需改堆栈大小 ， 此 `Workspace` 修改 `Stack/Heap Sizes`  - `CSTACK`  为 `0x2000`

5. 将 `Linux` 里面的 `pthread_mutex_lock` 接口替换为 `CRITICAL_SECTION_ENTER()`, `pthread_mutex_unlock ` 接口替换为  `CRITICAL_SECTION_LEAVE()` 。 函数实现是在 `mx_hal.h` 下：​	

   ```
   #define CRITICAL_SECTION_ENTER()        \
       {                                   \
           volatile hal_atomic_t __atomic; \
           atomic_enter_critical(&__atomic);
   
   #define CRITICAL_SECTION_LEAVE()      \
       atomic_leave_critical(&__atomic); \
       }
   ```

6. 运行 `at_test` `project`  

   - 可以在 debug uart ( PA9、PA10)  看到如下的输出：

     ```
     [APP: at_test.c:  54] emh init err = 0
     [APP: at_test.c:  58] FW version: ilop_AT_v2.1.4
     [APP: at_test.c:  59] System tick: 731
     ```

7. 运行 `ilopicaapp project`

   - 新烧录完整的  `Wi-Fi at ilop` 固件

   - 在 `example/ilopicaapp/ilop_main.c ` ,  修改 `TSL` 以及 `product_key` 、`product_secret` 、`device_secret` 、`device_name`
   - 借助 `cli` 命令 实现配网 ，可以在 `debug` 串口下 输入 `help`  查看 `cli` 命令

   -  烧写固件之后，输入 `cli` 调试指令 `aws press` , 进行配网操作，联网成功之后会主动上报当前状态，`app` 可以控制， 使用 `cli` 命令输入 `event enable`  可以打开主动上报 `event` 状态 。完整的 `log` 输出如下：

     ```
     [ILOP: alicloud_ilop.c: 140] FW version: ilop_AT_v2.1.4
     [ILOP: alicloud_ilop.c: 103] product key    :[a1MeLuIJPYL]
     [ILOP: alicloud_ilop.c: 104] product secret :[SegyIuWW6xBQGuIc]
     [ILOP: alicloud_ilop.c: 105] device secret  :[jyHwmwiyfo2s7DzvA6QG6R3VLMxXBghy]
     [ILOP: alicloud_ilop.c: 106] device name    :[light_ica]
     [ILOP: alicloud_ilop.c: 189] Wlan unconfigured, start config mode
     [ILOP: alicloud_ilop.c:  79] Wi-Fi config....
     aws press
     
     
     # [ILOP: alicloud_ilop.c: 250] Wlan event: STATION_UP
     [ILOP: alicloud_ilop.c:  83] Wi-Fi connected
     [ILOP: alicloud_ilop.c: 260] AliCloud event: CLOUD_CONNECT
     [ILOP: alicloud_ilop.c:  91] Cloud connected
     [ILOP: alicloud_ilop.c: 260] AliCloud event: LOCAL_CONNECT
     [ILOP: alicloud_ilop.c: 286] Set local attrs format ICA
     [ILOP: alicloud_ilop.c: 294] arg_list[0]: property arg_list[1]: LightSwitch arg_list[2]: 0  
     [APP: app_prop.c:  19] write LightSwitch:0
     [APP: app_prop.c:  27] read LightSwitch:0
     [APP: app_prop.c:  42] read ColorTemperature:4000
     [APP: app_prop.c:  57] read R:0
     [APP: app_prop.c:  72] read G:2
     [APP: app_prop.c:  87] read B:100
     [ILOP: ica_protocol.c: 123] Send Property to cloud 85 bytes > LightSwitch,0,ColorTemperature,4000,RGBColor.Red,0,RGBColor.Green,2,RGBColor.Blue,100
     event enable
     
     
     # [APP: app_event.c:  67] time[10]
     [APP: app_event.c:  25] read value:0
     [ILOP: ica_protocol.c: 131] Send Event to cloud 17 bytes > Error.ErrorCode,0
     [ILOP: alicloud_ilop.c: 286] Set local attrs format ICA
     [ILOP: alicloud_ilop.c: 294] arg_list[0]: property arg_list[1]: LightSwitch arg_list[2]: 1  
     [APP: app_prop.c:  19] write LightSwitch:1
     [APP: app_prop.c:  27] read LightSwitch:1
     [ILOP: ica_protocol.c: 123] Send Property to cloud 13 bytes > LightSwitch,1
     [ILOP: alicloud_ilop.c: 286] Set local attrs format ICA
     [ILOP: alicloud_ilop.c: 294] arg_list[0]: property arg_list[1]: LightSwitch arg_list[2]: 0  
     [APP: app_prop.c:  19] write LightSwitch:0
     [APP: app_prop.c:  27] read LightSwitch:0
     [ILOP: ica_protocol.c: 123] Send Property to cloud 13 bytes > LightSwitch,0
     [APP: app_event.c:  67] time[20]
     [APP: app_event.c:  25] read value:0
     [ILOP: ica_protocol.c: 131] Send Event to cloud 17 bytes > Error.ErrorCode,0
     ```

8. 运行 `iloprawapp  project` 

   - 新烧录完整的  Wi-Fi at ilop 固件

   - 在 `example/ilopicaapp/ilop_main.c ` ,  修改  `product_key` 、`product_secret` 、`device_secret` 、`device_name`
   - 借助 `cli` 命令 实现配网 ，可以在 `debug` 串口下 输入 `help`  查看 `cli` 命令

   -  烧写固件之后，输入 `aws press` , 进行配网操作，联网成功之后会主动上报当前状态，`app` 可以控制， 使用 `cli` 命令输入 `event enable`  可以打开主动上报 `event` 状态 。完整的 `log` 输出如下：

     ```
     [ILOP: alicloud_ilop.c: 140] FW version: ilop_AT_v2.1.4
     [ILOP: alicloud_ilop.c: 103] product key    :[b1CcQqrXoVR]
     [ILOP: alicloud_ilop.c: 104] product secret :[yOrZrmtDcHLgVOJc]
     [ILOP: alicloud_ilop.c: 105] device secret  :[d8wLGwb88lVNjd5PVUyuvy7GeQFzFNuv]
     [ILOP: alicloud_ilop.c: 106] device name    :[light_raw_m]
     [ILOP: alicloud_ilop.c: 189] Wlan unconfigured, start config mode
     [ILOP: alicloud_ilop.c:  79] Wi-Fi config....
     aws press
     
     
     # [ILOP: alicloud_ilop.c: 250] Wlan event: STATION_UP
     [ILOP: alicloud_ilop.c:  83] Wi-Fi connected
     [ILOP: alicloud_ilop.c: 260] AliCloud event: CLOUD_CONNECT
     [ILOP: alicloud_ilop.c:  91] Cloud connected
     [ILOP: alicloud_ilop.c: 260] AliCloud event: LOCAL_CONNECT
     [ILOP: raw_protocol.c: 437] local property
     [ILOP: raw_protocol.c: 339] add body attr_id:0
     [APP: app_prop_light.c:  22] read value:0
     [ILOP: raw_protocol.c: 339] add body attr_id:1
     [APP: app_prop_wifi.c:  16] read value:2.4G
     [ILOP: raw_protocol.c: 339] add body attr_id:2
     [APP: app_prop_wifi.c:  30] read value:-40
     [ILOP: raw_protocol.c: 339] add body attr_id:3
     [APP: app_prop_wifi.c:  44] read value:00:00:00:00:00:01
     [ILOP: raw_protocol.c: 339] add body attr_id:4
     [APP: app_prop_wifi.c:  58] read value:6
     [ILOP: raw_protocol.c: 339] add body attr_id:5
     [APP: app_prop_wifi.c:  72] read value:100
     [ILOP: raw_protocol.c: 339] add body attr_id:6
     [APP: app_prop_light.c:  38] read value:3000
     [ILOP: raw_protocol.c: 339] add body attr_id:7
     [APP: app_prop_light.c:  56] read[3] R[0] G[0] B[0]
     [ILOP: raw_protocol.c: 415] default
     
     AA003A010000000000000000000B010004322E34470102D80B03001130303A30303A30303A30303A30303A303102040601056404060BB81E0700000095
     [ILOP: alicloud_ilop.c: 299] Set local attrs format RAW
     
     AA000B008001088BA61D0000018D
     [ILOP: raw_protocol.c: 321] raw down data len:11
     [ILOP: raw_protocol.c: 296] raw method set
     [ILOP: raw_protocol.c: 221] find property index:0
     [APP: app_prop_light.c:  13] write value:1
     [ILOP: raw_protocol.c: 437] local property
     [ILOP: raw_protocol.c: 339] add body attr_id:0
     [APP: app_prop_light.c:  22] read value:1
     
     AA000B01000000000004000001BB
     [ILOP: alicloud_ilop.c: 299] Set local attrs format RAW
     
     AA000B008001088BA631000000A0
     [ILOP: raw_protocol.c: 321] raw down data len:11
     [ILOP: raw_protocol.c: 296] raw method set
     [ILOP: raw_protocol.c: 221] find property index:0
     [APP: app_prop_light.c:  13] write value:0
     [ILOP: raw_protocol.c: 437] local property
     [ILOP: raw_protocol.c: 339] add body attr_id:0
     [APP: app_prop_light.c:  22] read value:0
     
     AA000B01000000000008000000BE
     event enable
     
     
     # [APP: app_event.c:  67] time[10]
     [ILOP: raw_protocol.c: 501] local event
     [ILOP: raw_protocol.c: 339] add body attr_id:0
     [APP: app_event.c:  25] read value:0
     
     AA000B0100810000000C00000043
     [APP: app_event.c:  67] time[20]
     [ILOP: raw_protocol.c: 501] local event
     [ILOP: raw_protocol.c: 339] add body attr_id:0
     [APP: app_event.c:  25] read value:0
     
     AA000B0100810000001000000047
     ```



## 4.2 资源使用统计

### 4.2.1 `example - at_test` 

- 根据编译生层的 `map` 表分析：

![image-20181023143038226](https://ws2.sinaimg.cn/large/006tNbRwgy1fwi56ir2w2j30dl06u74s.jpg)



- [ ] 	 `readonly  code memory` 和 `readonly  data memory` 占用 `flash` 容量 
- [ ]  `  readwrite data memory` 占用 `ram` 容量

- 计算 `iot-sdk` 本身占用的资源 ，需要除去 `NUCLEO-F411RE` 平台占用的资源 ，可以在 `.map` 文件上查看  `MODULE SUMMARY` 模块，这部分会将各个 `.o` 文件占用的 `ro` 代码大小、`rw` 数据大小以 `list` 方式列出来。

- `iot-sdk example - at_test` 本身大约占用资源： 

  - [ ] `Flash: 25680 bytes(rocode) + 2218bytes(rodata) = 28798 bytes = 28k`  

  - [ ] `Ram:12924bytes = 12.6k`


### 4.2.2 `example - ilopicaapp`

类似的，`iot-sdk example - ilopicaapp` 本身大约占用资源： 

- [ ] `Flash: 45.7k`  
- [ ] `Ram:13k`



### 4.2.3 `example - iloprawapp`

类似的，`iot-sdk example - iloprawapp` 本身大约占用资源： 

- [ ] `Flash: 44.6k`  
- [ ] `Ram:13k`





## 5. 移植SDK

- 只需要实现platform/mx_hal.h文件中的函数即可
